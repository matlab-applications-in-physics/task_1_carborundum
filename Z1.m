%MATLAB 2020b

 %name: Task 1

 %author:Carborundum

 %date: 8.11.2020

 %version: v1.0

%p1

h=6.62607015e-34

h/(2*pi)

%p2

e=exp(1)

sin(((pi)/4)/e)

%p3
f=hex2dec('0x0098d6')/(1.445*1e23)

%p4

sqrt(e-(pi))

%p5

%p6

mr=21*365*24+21*24+4*31*24+2*30*24+8*24+11

%p7

r=6371000
a=e^((sqrt(7)/2)-log(r/(10^8)))
b=hex2dec('0xaaff')
c=atan(a/b)

%p8

p=6.02*1E23
e=(1/4)*(1E-6)*p

%p9

i=(2*e)/100
j=(i/(2*e))*1000
